# Changelog

## 3.2.1 - 2025-02-25

### Fixed

- Chaînes de langue au format SPIP 4.1+

## 3.2.0 - 2025-01-27

### Changed

- #28 Compatibilité SPIP 4.*

## 3.1.3 — 2025-01-10

### Changed

- La fonction `Crayons_preparer_page` est déplacée dans inc `inc/crayons_preparer_page.php` et devient une fonction `_dist`
- #22 Améliorer la chaîne de langue pour activer les crayons dans le privé

### Fixed

- #27 Ne plus utiliser la globale `auteur_session` obsolete


## 3.1.2 — 2023-06-09

### Fixed

- Compatibilité SPIP 4.3

## 3.1.1 — 2023-10-11

### Fixed

- #17 Réparer la couleur de fond des champs des crayons
- Correction d'une coquille CSS

## 3.1.0 — 2023-06-05

### Changed

- #3 Éviter d’activer les crayons au scroll sur mobile
- Upgrade en jQuery 3.7.0 (1.12 avant)
- Le cache JS de crayons va dans `local/cache-js` (et pas `local/` tout court)

### 3.0.0 — 2023-03-15

## Changed

- Compatible SPIP 4.1 minimum
- Compatible SPIP 4.2
